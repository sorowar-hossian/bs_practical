﻿using BSPracticalTest.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;


namespace BSPracticalTest.DbConnetion
{
    public class GetDbContext : BSTestDbEntities
    {
       
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public DbSet<tblUser> Users { get; set; }
        public DbSet<tblPost> Posts { get; set; }
        public DbSet<tblComment> Comments { get; set; }
    }
}