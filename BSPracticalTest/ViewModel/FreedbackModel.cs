﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BSPracticalTest.ViewModel
{
    public class FreedbackModel
    {
        public string Post { get; set; }
        public string Postman { get; set; }
        public DateTime PostDate { get; set; }
        public int CommentCount { get; set; }
        public List<VMCommentModel> Comments = new List<VMCommentModel>();
    }
}