﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BSPracticalTest.ViewModel
{
    public class VMCommentModel
    {
        public string Comment { get; set; }
        public string CommentUser { get; set; }
        public DateTime CommentDate { get; set; }
        public int like { get; set; }
        public int Dislike { get; set; }
       
    }
}