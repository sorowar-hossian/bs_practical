USE [master]
GO
/****** Object:  Database [BSTestDb]    Script Date: 12/5/2020 11:21:12 AM ******/
CREATE DATABASE [BSTestDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BSTestDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BSTestDb.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BSTestDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BSTestDb_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BSTestDb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BSTestDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BSTestDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BSTestDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BSTestDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BSTestDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BSTestDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [BSTestDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BSTestDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BSTestDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BSTestDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BSTestDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BSTestDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BSTestDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BSTestDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BSTestDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BSTestDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BSTestDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BSTestDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BSTestDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BSTestDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BSTestDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BSTestDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BSTestDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BSTestDb] SET RECOVERY FULL 
GO
ALTER DATABASE [BSTestDb] SET  MULTI_USER 
GO
ALTER DATABASE [BSTestDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BSTestDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BSTestDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BSTestDb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BSTestDb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BSTestDb', N'ON'
GO
USE [BSTestDb]
GO
/****** Object:  Table [dbo].[tblComments]    Script Date: 12/5/2020 11:21:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblComments](
	[ComId] [int] IDENTITY(1,1) NOT NULL,
	[Comment] [varchar](1000) NULL,
	[CommentLike] [int] NULL,
	[CommentDislike] [int] NULL,
	[PostId] [int] NULL,
	[UserId] [int] NULL,
	[CommentDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ComId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPost]    Script Date: 12/5/2020 11:21:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPost](
	[PostId] [int] IDENTITY(1,1) NOT NULL,
	[Post] [varchar](1000) NULL,
	[PostDate] [datetime] NULL,
	[UserId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 12/5/2020 11:21:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUsers](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](100) NULL,
	[UserRole] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblComments] ON 

INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (1, N'comment1', 150, 50, 1, 2, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (2, N'comment2', 250, 70, 1, 3, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (3, N'comment3', 250, 70, 1, 4, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (4, N'comment4', 250, 70, 2, 2, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (5, N'comment5', 78, 100, 2, 3, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (6, N'comment6', 78, 100, 2, 4, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (7, N'comment8', 200, 100, 3, 2, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
INSERT [dbo].[tblComments] ([ComId], [Comment], [CommentLike], [CommentDislike], [PostId], [UserId], [CommentDate]) VALUES (8, N'comment7', 200, 100, 2, 3, CAST(N'2020-12-05 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[tblComments] OFF
SET IDENTITY_INSERT [dbo].[tblPost] ON 

INSERT [dbo].[tblPost] ([PostId], [Post], [PostDate], [UserId]) VALUES (1, N'post1', CAST(N'2020-12-05 00:31:50.270' AS DateTime), 1)
INSERT [dbo].[tblPost] ([PostId], [Post], [PostDate], [UserId]) VALUES (2, N'post2', CAST(N'2020-12-05 00:31:58.373' AS DateTime), 1)
INSERT [dbo].[tblPost] ([PostId], [Post], [PostDate], [UserId]) VALUES (3, N'post3', CAST(N'2020-12-05 00:32:07.647' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblPost] OFF
SET IDENTITY_INSERT [dbo].[tblUsers] ON 

INSERT [dbo].[tblUsers] ([UserId], [UserName], [UserRole]) VALUES (1, N'admin', N'admin')
INSERT [dbo].[tblUsers] ([UserId], [UserName], [UserRole]) VALUES (2, N'user1', N'user1')
INSERT [dbo].[tblUsers] ([UserId], [UserName], [UserRole]) VALUES (3, N'user2', N'user2')
INSERT [dbo].[tblUsers] ([UserId], [UserName], [UserRole]) VALUES (4, N'user3', N'user3')
SET IDENTITY_INSERT [dbo].[tblUsers] OFF
ALTER TABLE [dbo].[tblComments]  WITH CHECK ADD FOREIGN KEY([PostId])
REFERENCES [dbo].[tblPost] ([PostId])
GO
ALTER TABLE [dbo].[tblComments]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUsers] ([UserId])
GO
ALTER TABLE [dbo].[tblPost]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[tblUsers] ([UserId])
GO
USE [master]
GO
ALTER DATABASE [BSTestDb] SET  READ_WRITE 
GO
