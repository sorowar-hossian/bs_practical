//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BSPracticalTest.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblComment
    {
        public int ComId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CommentLike { get; set; }
        public Nullable<int> CommentDislike { get; set; }
        public Nullable<int> PostId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> CommentDate { get; set; }
    
        public virtual tblPost tblPost { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}
