//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BSPracticalTest.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPost
    {
        public tblPost()
        {
            this.tblComments = new HashSet<tblComment>();
        }
    
        public int PostId { get; set; }
        public string Post { get; set; }
        public Nullable<System.DateTime> PostDate { get; set; }
        public Nullable<int> UserId { get; set; }
    
        public virtual tblUser tblUser { get; set; }
        public virtual ICollection<tblComment> tblComments { get; set; }
    }
}
