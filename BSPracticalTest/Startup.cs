﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BSPracticalTest.Startup))]
namespace BSPracticalTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
