﻿using BSPracticalTest.DbConnetion;
using BSPracticalTest.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;


namespace BSPracticalTest.Controllers
{
    public class WebApiController : Controller
    {
        private GetDbContext _context = new GetDbContext();

        [HttpPost]
        [Route("api/GetAllFreedBack")]
        [HttpGet]
        public ActionResult Get( string search, int page = 1, int pageSize = 3)
        {
            try
            {
            List<FreedbackModel> aList = new List<FreedbackModel>();
          

            var posts = (
                            from p in _context.Posts
                            join u in _context.Users on p.UserId equals u.UserId


                            select new
                            {
                                p.Post,
                                p.PostId,
                                p.PostDate,
                                u.UserName
                            }).ToList();

            var comments = (from c in _context.Comments
                            join u in _context.Users on c.UserId equals u.UserId
                            select new
                            {
                                c.PostId,
                                c.Comment,
                                c.CommentDislike,
                                c.CommentLike,
                                u.UserName,
                                c.ComId,
                                c.CommentDate
                            }).ToList();

            foreach (var item in posts)
            {
                var obj = new FreedbackModel();
                obj.Post = item.Post;
                obj.Postman = item.UserName;
                obj.PostDate = item.PostDate ?? DateTime.MinValue;
                obj.CommentCount = comments.Count(s => s.PostId == item.PostId);

                foreach (var item2 in comments.Where(id => id.PostId == item.PostId))
                {
                    var comt = new VMCommentModel();
                    comt.Comment = item2.Comment;
                    comt.CommentUser = item2.UserName;
                    comt.CommentDate = item2.CommentDate ?? DateTime.MinValue;
                    comt.like = item2.CommentLike ?? 0;
                    comt.Dislike = item2.CommentDislike ?? 0;

                    obj.Comments.Add(comt);
                }

                aList.Add(obj);
            }
          
            if (!string.IsNullOrEmpty(search))
            {
                aList = aList.Where(c => c.Post.ToLower().Contains(search.ToLower())).ToList();
            }

            int pageNumber = (page);
            aList.ToPagedList(pageNumber, pageSize);



            return Json(new { success = true, data = aList, message = "successful.." });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

    
    }
}
