﻿using BSPracticalTest.DbConnetion;
using BSPracticalTest.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace BSPracticalTest.Controllers
{
    public class HomeController : Controller
    {
        private GetDbContext _context = new GetDbContext();
        public ActionResult Index()
        {
            List<FreedbackModel> aList = new List<FreedbackModel>();
       

            var posts = (
                            from p in _context.Posts
                            join u in _context.Users on p.UserId equals u.UserId


                            select new
                            {
                                p.Post,
                                p.PostId,
                                p.PostDate,
                                u.UserName
                            }).ToList();

            var comments = (from c in _context.Comments
                            join u in _context.Users on c.UserId equals u.UserId
                            select new
                            {
                                c.PostId,
                                c.Comment,
                                c.CommentDislike,
                                c.CommentLike,
                                u.UserName,
                                c.ComId,
                                c.CommentDate
                            } ).ToList();

            foreach (var item in posts)
            {
                var obj = new FreedbackModel();
                obj.Post = item.Post;
                obj.Postman = item.UserName;
                obj.PostDate = item.PostDate ?? DateTime.MinValue;
                obj.CommentCount = comments.Count(s => s.PostId == item.PostId);

                foreach (var item2 in comments.Where(id => id.PostId == item.PostId))
                {
                   var comt = new VMCommentModel();
                    comt.Comment = item2.Comment;
                    comt.CommentUser = item2.UserName;
                    comt.CommentDate = item2.CommentDate ?? DateTime.MinValue;
                    comt.like = item2.CommentLike ?? 0;
                    comt.Dislike = item2.CommentDislike ?? 0;

                    obj.Comments.Add(comt);
                }

                aList.Add(obj);
            }
            string search = "";
            if (!string.IsNullOrEmpty(search))
            {
                aList = aList.Where(c => c.Post.ToLower().Contains(search.ToLower())).ToList();
            }

            int pageSize = 3;
            int page = 10;
            int pageNumber = (page);
            aList.ToPagedList(pageNumber, pageSize);
            
            
            
            
            
            
            
            
            var freedback = (from post in
                                 (
                                     from p in _context.Posts
                                     join u in _context.Users on p.UserId equals u.UserId


                                     select new
                                     {
                                         p.Post,
                                         p.PostId,
                                         p.PostDate,
                                         u.UserName
                                     })
                             join comment in
                                 (
                                     from c in _context.Comments
                                     join u in _context.Users on c.UserId equals u.UserId
                                     select new
                                     {
                                         c.PostId,
                                         c.Comment,
                                         c.CommentDislike,
                                         c.CommentLike,
                                         u.UserName,
                                         c.ComId
                                     }
                                  ) on post.PostId equals comment.PostId


                             select new
                             {
                                 post.PostId,
                                 post.Post,
                                 comment.Comment,
                                 comment.UserName,
                                 post.PostDate,
                                 comment.ComId

                             }

                           ).GroupBy(g => new { g.PostId, g.ComId }).ToList();








            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}